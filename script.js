const mdFilesList = ["md/cover.md", "md/main.md", "md/back.md"];
const partsList = ["cover", 'content', "back"];

const markdownit = window.markdownit(
    {
        // Options for markdownit
        langPrefix: 'language-fr',
        // You can use html markup element
        html: true,
        typographer: true,
        // Replace english quotation by french quotation
        quotes: ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'],
    })
    .use(markdownitContainer)
    .use(markdownitSpan)
    .use(markdownItAttrs, {
        // optional, these are default options
        leftDelimiter: '{',
        rightDelimiter: '}',
        allowedAttributes: [] // empty array = all attributes are allowed
    });

function init() {

    mdFilesList.forEach((mdFile, index) => {

        const request = new XMLHttpRequest();
        request.open("GET", mdFile);

        request.addEventListener("readystatechange", function (event) {

            if (request.readyState === XMLHttpRequest.DONE && request.status === 200) {

                const response = request.responseText;
                const result = markdownit.render(response);

                document.getElementById(partsList[index]).innerHTML = result;

                //if we have all the md files loaded, call Paged.js
                if (index === (mdFilesList.length) - 1) {
                    loadScript("vendors/js/paged.polyfill.js");

                    //add the paged.js css
                    const pagedCss = document.createElement("link");
                    pagedCss.href = "vendors/css/paged-preview.css";
                    pagedCss.type = "text/css";
                    pagedCss.rel = "stylesheet";
                    document.head.appendChild(pagedCss);
                }
            }
        });

        request.send();
    });
}

init();

//utils functions

// Load a script with a promise
function loadScript(src) {
    return new Promise(function (resolve, reject) {
        const script = document.createElement('script');
        script.src = src;

        script.onload = () => resolve(script);
        script.onerror = () => reject(new Error(`Script load error for ${src}`));

        document.body.appendChild(script);
    });
}
